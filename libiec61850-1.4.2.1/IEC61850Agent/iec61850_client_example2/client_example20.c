/*
 * client_example2.c
 *
 * This example shows how to browse the data model of an unknown device.
 */

#include "iec61850_client.h"
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>





int
main(int argc, char** argv)
{

    char* hostname;
    int tcpPort = 102;

    if (argc > 1)
        hostname = argv[1];
    else
        hostname = "localhost";

    if (argc > 2)
        tcpPort = atoi(argv[2]);

    IedClientError error;

    IedConnection con = IedConnection_create();

    IedConnection_connect(con, &error, hostname, tcpPort);

    if (error == IED_ERROR_OK) {

        printf("Get logical device list...\n");
        LinkedList deviceList = IedConnection_getLogicalDeviceList(con, &error);

        if (error != IED_ERROR_OK) {
            printf("Failed to read device list (error code: %i)\n", error);
            goto cleanup_and_exit;
        }

        LinkedList device = LinkedList_getNext(deviceList);

        char *buf;
        int  len = 0;

        while (device != NULL) {

            buf[0] = '{';
            len++;
            
             LinkedList logicalNodes = IedConnection_getLogicalDeviceDirectory(con, &error,(char*) device->data);

            //salva LOgical Divice
            len += sprintf(&buf[len],"\n    \"name\" : \"%s\",\n    \"type\" : \"LD\"", (char*) device->data  );

            
            //
            
            // LinkedList logicalNode = LinkedList_getNext(logicalNodes);



            // if (logicalNode == NULL) 
             len += sprintf(&buf[len], ",\n    \"items\" : [ ]");
            // else 
             len += sprintf(&buf[len], ",\n    \"items\" : [");

            // while (logicalNode != NULL) {


            //     len += sprintf(&buf[len],"\n    Algumacoisa");
                
                
            //     //Final para o loop
            //     logicalNode = LinkedList_getNext(logicalNode);
            // }




            
 
            

            len += sprintf(&buf[len],"\n");
            len += sprintf(&buf[len],"]");

             //buf[len] = '\n';
	         //len++;
            // buf[len] = ']';
	        // len++;



            //LinkedList_destroy(logicalNodes);
            device = LinkedList_getNext(device);
        }

        //buf[len] = '\n';
	    //len++;
	    //buf[len] = '}';
	    //len++;


        printf("Json:\n%s  TamanhoLEN: %i...%li\n", buf, len, strlen(buf) );


        LinkedList_destroy(deviceList);
        IedConnection_close(con);
    }
    else {
        printf("Connection failed!\n");
    }

cleanup_and_exit:
    IedConnection_destroy(con);
}

