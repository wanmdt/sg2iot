
from flask import Flask, request, jsonify, make_response, abort
from flask_request_params import bind_request_params
import requests
import json
import re
import os
import subprocess
import time
import datetime 
import threading
import gc 

global stringg

# flask variables
app = Flask(__name__)
app.before_request(bind_request_params)


@app.errorhandler(404)
def not_found(error):
    # custom error message
    return make_response(jsonify({'error': 'Not found'}), 404)


@app.errorhandler(400)
def bad_request(error):
    return make_response(jsonify({'error': 'bad request'}), 400)

def registrar_time_message(mens):
    arquivo = open("DataHanlerFlask.txt", "a")
    # dataT = datetime.datetime.now()
    arquivo.write(str(datetime.datetime.now())+' - '+mens+'\n')
    arquivo.close()


@app.route('/datahandler/DATA/', methods=['GET', 'POST'])
def data():
    print(gc.get_count())
    #stringg = ''
    if request.method == 'POST':
        stringg = request.data
        result = re.split(r'\w+\:\s', stringg.strip())
        seq = request.args.get('seq')
        mens = ' ReceiveIEDDATA - ID: '+result[1].strip()+' seq: '+str(seq)+' length: '+str(len(stringg))+' deviceip: '+result[2].strip()+' entitytype: '+result[3].strip()
        registrar_time_message(mens) #save txt
        
        try:
            payload = {'seq': str(seq)}
            mens = ' SendIEDDATAGT - ID: '+result[1].strip()+' seq: '+str(seq)+' length: '+str(len(stringg))+' deviceip: '+result[2].strip()+' entitytype: '+result[3].strip()
            registrar_time_message(mens) #save txt
            post = requests.post('http://127.0.0.1:5015/gateway/DATA/', params=payload, data=stringg)
            mens = ' SendIEDDATAGTOK - ID: '+result[1].strip()+' seq: '+str(seq)+' length: '+str(len(stringg))+' deviceip: '+result[2].strip()+' entitytype: '+result[3].strip()
            registrar_time_message(mens) #save txt
            result = ''
            mens = ''
            # print (post.status_code)

        except requests.exceptions.RequestException as e:  # This is the correct syntax
            print (e)


        # print(stringg)
        return stringg
    if request.method == 'GET':
        stringg = "Not permited"
        return stringg


   


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5010)

