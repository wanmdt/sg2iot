#-*- coding: utf-8 -*-

from flask import Flask, request, jsonify, make_response, abort
from flask_request_params import bind_request_params
import requests
import json
import re
import subprocess
import time
import datetime
import threading
import sys
import gc 
 

hostname = ""
STRING_TOTAL = ""
portStr = ""

if (sys.argv > 1):
    hostname = sys.argv[1]
else:
    hostname = "127.0.0.1"

if (sys.argv > 2):
    STRING_TOTAL = sys.argv[2]

if (sys.argv > 3):
    portStr = sys.argv[3]


def registrar_time_message(mens):
    arquivo = open("IED.txt", "a")
    arquivo.write(str(datetime.datetime.now())+' - '+mens+'\n')
    arquivo.close()


def dataValueIEC61850(hostname, seq):
    string_JSON = "{"
    string_JSON_FI = "{"

    string_JSON_FI00 = "{"

    string_JSON_FI_VALUE_ALL = ""
    string_JSON_FI_VALUE_DA = ""

    
    u = 'http://'+hostname+':'+portStr+'/IEC61850/ALL/'
    #urlALL = "http://127.0.0.1:5020/DNP3/ULTRA/"


    result = re.split(r'\w+\:\s', STRING_TOTAL.strip())
    mens = ' ReadIEDDATA - ID: '+result[1].strip()+' seq: '+str(seq)+' length: '+str(len(u))+' deviceip: '+result[2].strip()+' entitytype: '+result[3].strip()
    urlALL = u
    registrar_time_message(mens) #save txt
    rALL = requests.get(urlALL)
    mens = ''
    if rALL.status_code == 200:
        mens = ' ReadIEDDATAOK - ID: '+result[1].strip()+' seq: '+str(seq)+' length: '+str(len(rALL.text))+' deviceip: '+result[2].strip()+' entitytype: '+result[3].strip()
        registrar_time_message(mens)
        mens = ''
        dadoString_ALL = rALL.text
        # arquivo = open("iec61850.txt", "a")
        # arquivo.write(dadoString_ALL+'\n\n\n')

        #return dadoString_ALL
        string_JSON_TOTAL = STRING_TOTAL+ dadoString_ALL
        # print(string_JSON_TOTAL)
        try:
            payload = {'seq': str(seq)}
            mens = ' SendIEDDATADH - ID: '+result[1].strip()+' seq: '+str(seq)+' length: '+str(len(dadoString_ALL))+' deviceip: '+result[2].strip()+' entitytype: '+result[3].strip()
            registrar_time_message(mens)
            post = requests.post('http://127.0.0.1:5010/datahandler/DATA/', params=payload, data=string_JSON_TOTAL)
            mens = ' SendIEDDATADHOK - ID: '+result[1].strip()+' seq: '+str(seq)+' length: '+str(len(string_JSON_TOTAL))+' deviceip: '+result[2].strip()+' entitytype: '+result[3].strip()
            registrar_time_message(mens)
            mens = ''
            # print (post.status_code)
            # arquivo.write('\n03 '+string_JSON_TOTAL+'\n\n\n')            
        except requests.exceptions.RequestException as e:  # This is the correct syntax
            print (e)
    else:
        #print(rALL.status_code)
        stri = str(rALL.status_code)
        return stri
    
    result = ''


def dataValueIEC61850Power(hostname, seq):

    string_JSON = "{"
    string_JSON_FI = "{"

    string_JSON_FI00 = "{"

    string_JSON_FI_VALUE_ALL = ""
    string_JSON_FI_VALUE_DA = ""

    
    u = 'http://'+hostname+':'+portStr+'/IEC61850/POWER/'
    #urlALL = "http://127.0.0.1:5020/DNP3/ULTRA/"


    result = re.split(r'\w+\:\s', STRING_TOTAL.strip())
    mens = ' ReadIEDDATA - ID: '+result[1].strip()+' seq: '+str(seq)+' length: '+str(len(u))+' deviceip: '+result[2].strip()+' entitytype: '+result[3].strip()
    urlALL = u
    registrar_time_message(mens) #save txt
    rALL = requests.get(urlALL)
    mens = ''
    if rALL.status_code == 200:
        mens = ' ReadIEDDATAOK - ID: '+result[1].strip()+' seq: '+str(seq)+' length: '+str(len(rALL.text))+' deviceip: '+result[2].strip()+' entitytype: '+result[3].strip()
        registrar_time_message(mens)
        mens = ''
        dadoString_ALL = rALL.text
        # arquivo = open("iec61850.txt", "a")
        # arquivo.write(dadoString_ALL+'\n\n\n')

        #return dadoString_ALL
        string_JSON_FI00 = "NONE"
        string_JSON_TOTAL = STRING_TOTAL+ ' VARIABLES: '+string_JSON_FI00+' POWER: '+ dadoString_ALL
        # print(string_JSON_TOTAL)
        try:
            payload = {'seq': str(seq)}
            mens = ' SendIEDDATADH - ID: '+result[1].strip()+' seq: '+str(seq)+' length: '+str(len(dadoString_ALL))+' deviceip: '+result[2].strip()+' entitytype: '+result[3].strip()
            registrar_time_message(mens)
            post = requests.post('http://127.0.0.1:5010/datahandler/DATA/', params=payload, data=string_JSON_TOTAL)
            mens = ' SendIEDDATADHOK - ID: '+result[1].strip()+' seq: '+str(seq)+' length: '+str(len(string_JSON_TOTAL))+' deviceip: '+result[2].strip()+' entitytype: '+result[3].strip()
            registrar_time_message(mens)
            mens = ''
            # print (post.status_code)
            # arquivo.write('\n03 '+string_JSON_TOTAL+'\n\n\n')            
        except requests.exceptions.RequestException as e:  # This is the correct syntax
            print (e)
    else:
        #print(rALL.status_code)
        stri = str(rALL.status_code)
        return stri
    
    result = ''





    
def loop():
    # arquivo = open("iec61850envio.txt", "a")
    seq = 1
    cc = 0
    while True:

        if cc == 0:
            dataValueIEC61850(hostname, seq)
            seq = seq +1
            cc = cc + 1
            time.sleep(1)
        else:
            dataValueIEC61850Power(hostname, seq)
            seq = seq +1
            time.sleep(1)

            
        print(gc.get_count())
        # print(gc.collect())
        # print(gc.get_count())


if __name__ == '__main__':
    loop()
    # thr_collection = threading.Thread(target=loop)
    # thr_collection.start()
    #app.run(debug=True, host='0.0.0.0', port=tcpPort)
