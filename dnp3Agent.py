#!/usr/bin/env python
#-*- coding: utf-8 -*-

from flask import Flask, request, jsonify, make_response, abort
from flask_request_params import bind_request_params
import requests
import json
import re
import subprocess
import time
import datetime
import sys
import threading
import gc 

hostname = ""
STRING_TOTAL = ""
portStr = ""

if (sys.argv > 1):
    hostname = sys.argv[1]
else:
    hostname = "127.0.0.1"

if (sys.argv > 2):
    STRING_TOTAL = sys.argv[2]

if (sys.argv > 3):
    portStr = sys.argv[3]


def registrar_time_message(mens):
    arquivo = open("IED.txt", "a")
    #dataT = datetime.datetime.now()
    arquivo.write(str(datetime.datetime.now())+' - '+mens+'\n')
    arquivo.close()



string_JSON = "{"
string_JSON_FI = "{"



string_JSON_FI_VALUE_ALL = ""

index = 0

value = 0
index = 0
tipo = ""
date_time = ""

global string_JSON_TOTAL
string_JSON_TOTAL =''

# @app.route('/datahandler/DNP3/ALL/')
# def dataValueAll():
#     return string_JSON_FI_VALUE_ALL

# @app.route('/datahandler/DNP3/DATA/JSON/')
# def dataValueDataJSON():
#     u = 'http://'+hostname+':5020/DNP3/JSON/'
#     urlALL = u
#     rALL = requests.get(urlALL)
#     if rALL.status_code == 200:
#         dadoString_ALL = rALL.text

#         return dadoString_ALL
#     else:
#         #print(rALL.status_code)
#         stri = str(rALL.status_code)
#         return stri


# @app.route('/datahandler/DNP3/DATA/ULTRA/')
def dataValueDataUltra(seq):
    portFlask = portStr
    u = 'http://'+hostname+':'+portFlask+'/DNP3/ULTRA/'
    #urlALL = "http://127.0.0.1:5020/DNP3/ULTRA/"

    result = re.split(r'\w+\:\s', STRING_TOTAL.strip())
    mens = ' ReadIEDDATA - ID: '+result[1].strip()+' seq: '+str(seq)+' length: '+str(len(u))+' deviceip: '+result[2].strip()+' entitytype: '+result[3].strip()
    urlALL = u
    rALL = requests.get(urlALL)
    registrar_time_message(mens) #save txt
    mens = ''
    if rALL.status_code == 200:
        mens = ' ReadIEDDATAOK - ID: '+result[1].strip()+' seq: '+str(seq)+' length: '+str(len(rALL.text))+' deviceip: '+result[2].strip()+' entitytype: '+result[3].strip()
        registrar_time_message(mens)
        mens = ''
        dadoString_ALL = rALL.text

        return dadoString_ALL
    else:
        #print(rALL.status_code)
        stri = str(rALL.status_code)
        return stri
    
    result = ''

# @app.route('/datahandler/DNP3/variables/')
# def datavariables():
#     return string_JSON_FI00

    


string_JSON_FI00 = "{\"object_id\" : \"value\",\"name\" : \"value\",\"type\" : \"Analog measurement\"},{\"object_id\" : \"index\",\"name\" : \"index\",\"type\" : \"Text\"},{\"object_id\" : \"tipo\",\"name\" : \"tipo\",\"type\" : \"Text\"},{\"object_id\" : \"date_time\",\"name\" : \"date_time\",\"type\" : \"date\"}"




#print(string_JSON_FI00)


def send_data():
    seq = 1
    

    while True:
        string_JSON_TOTAL = STRING_TOTAL+ ' VARIABLES: '+string_JSON_FI00+' ULTRA: '+ dataValueDataUltra(seq)
        #dataUltra = dataValueDataUltra()
        # men = str(datetime.datetime.now())+ " - 02 - dnp3Agent " + "getDataFromIED"
        # registrar_time_message(men)
        # print(string_JSON_TOTAL)
        try:
            payload = {'seq': str(seq)}
            result = re.split(r'\w+\:\s', STRING_TOTAL.strip())
            mens = ' SendIEDDATADH - ID: '+result[1].strip()+' seq: '+str(seq)+' length: '+str(len(string_JSON_TOTAL))+' deviceip: '+result[2].strip()+' entitytype: '+result[3].strip()
            registrar_time_message(mens) # save time txt
            post = requests.post('http://127.0.0.1:5010/datahandler/DATA/', params=payload, data=string_JSON_TOTAL)
            mens = ' SendIEDDATADHOK - ID: '+result[1].strip()+' seq: '+str(seq)+' length: '+str(len(string_JSON_TOTAL))+' deviceip: '+result[2].strip()+' entitytype: '+result[3].strip()
            registrar_time_message(mens)
            mens = ''
            # print (post.status_code)
        except requests.exceptions.RequestException as e:  # This is the correct syntax
            print (e)
        seq = seq + 1
        time.sleep(1)
        print(gc.get_count())




if __name__ == '__main__':
    send_data()
    # thr_collection = threading.Thread(target=send_data)



