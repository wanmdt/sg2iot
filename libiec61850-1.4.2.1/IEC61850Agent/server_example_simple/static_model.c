/*
 * static_model.c
 *
 * automatically generated from scd.xml
 */
#include "static_model.h"

static void initializeValues();

extern DataSet iedModelds_C1_LLN0_Performance;
extern DataSet iedModelds_C1_LLN0_Positions;
extern DataSet iedModelds_C1_LLN0_Measurands;
extern DataSet iedModelds_C1_LLN0_smv;
extern DataSet iedModelds_C1_LLN0_rmxu;


extern DataSetEntry iedModelds_C1_LLN0_Performance_fcda0;
extern DataSetEntry iedModelds_C1_LLN0_Performance_fcda1;

DataSetEntry iedModelds_C1_LLN0_Performance_fcda0 = {
  "C1",
  false,
  "MMXU1$ST$Amps",
  -1,
  NULL,
  NULL,
  &iedModelds_C1_LLN0_Performance_fcda1
};

DataSetEntry iedModelds_C1_LLN0_Performance_fcda1 = {
  "C1",
  false,
  "MMXU1$ST$Volts",
  -1,
  NULL,
  NULL,
  NULL
};

DataSet iedModelds_C1_LLN0_Performance = {
  "C1",
  "LLN0$Performance",
  2,
  &iedModelds_C1_LLN0_Performance_fcda0,
  &iedModelds_C1_LLN0_Positions
};

extern DataSetEntry iedModelds_C1_LLN0_Positions_fcda0;
extern DataSetEntry iedModelds_C1_LLN0_Positions_fcda1;
extern DataSetEntry iedModelds_C1_LLN0_Positions_fcda2;
extern DataSetEntry iedModelds_C1_LLN0_Positions_fcda3;

DataSetEntry iedModelds_C1_LLN0_Positions_fcda0 = {
  "C1",
  false,
  "TVTR1$MX$Vol$instMag",
  -1,
  NULL,
  NULL,
  &iedModelds_C1_LLN0_Positions_fcda1
};

DataSetEntry iedModelds_C1_LLN0_Positions_fcda1 = {
  "C1",
  false,
  "CSWI1$ST$Pos",
  -1,
  NULL,
  NULL,
  &iedModelds_C1_LLN0_Positions_fcda2
};

DataSetEntry iedModelds_C1_LLN0_Positions_fcda2 = {
  "C1",
  false,
  "CSWI2$ST$Pos",
  -1,
  NULL,
  NULL,
  &iedModelds_C1_LLN0_Positions_fcda3
};

DataSetEntry iedModelds_C1_LLN0_Positions_fcda3 = {
  "C1",
  false,
  "MMXU1$MX$Mod$stVal",
  -1,
  NULL,
  NULL,
  NULL
};

DataSet iedModelds_C1_LLN0_Positions = {
  "C1",
  "LLN0$Positions",
  4,
  &iedModelds_C1_LLN0_Positions_fcda0,
  &iedModelds_C1_LLN0_Measurands
};

extern DataSetEntry iedModelds_C1_LLN0_Measurands_fcda0;

DataSetEntry iedModelds_C1_LLN0_Measurands_fcda0 = {
  "C1",
  false,
  "TVTR1$MX$Vol$instMag",
  -1,
  NULL,
  NULL,
  NULL
};

DataSet iedModelds_C1_LLN0_Measurands = {
  "C1",
  "LLN0$Measurands",
  1,
  &iedModelds_C1_LLN0_Measurands_fcda0,
  &iedModelds_C1_LLN0_smv
};

extern DataSetEntry iedModelds_C1_LLN0_smv_fcda0;
extern DataSetEntry iedModelds_C1_LLN0_smv_fcda1;
extern DataSetEntry iedModelds_C1_LLN0_smv_fcda2;

DataSetEntry iedModelds_C1_LLN0_smv_fcda0 = {
  "C1",
  false,
  "TVTR1$MX$Vol$instMag",
  -1,
  NULL,
  NULL,
  &iedModelds_C1_LLN0_smv_fcda1
};

DataSetEntry iedModelds_C1_LLN0_smv_fcda1 = {
  "C1",
  false,
  "CSWI1$MX$Mod",
  -1,
  NULL,
  NULL,
  &iedModelds_C1_LLN0_smv_fcda2
};

DataSetEntry iedModelds_C1_LLN0_smv_fcda2 = {
  "C1",
  false,
  "MMXU1$MX$Mod$stVal",
  -1,
  NULL,
  NULL,
  NULL
};

DataSet iedModelds_C1_LLN0_smv = {
  "C1",
  "LLN0$smv",
  3,
  &iedModelds_C1_LLN0_smv_fcda0,
  &iedModelds_C1_LLN0_rmxu
};

extern DataSetEntry iedModelds_C1_LLN0_rmxu_fcda0;
extern DataSetEntry iedModelds_C1_LLN0_rmxu_fcda1;
extern DataSetEntry iedModelds_C1_LLN0_rmxu_fcda2;

DataSetEntry iedModelds_C1_LLN0_rmxu_fcda0 = {
  "C1",
  false,
  "RMXU1$MX$AmpLocPhsA",
  -1,
  NULL,
  NULL,
  &iedModelds_C1_LLN0_rmxu_fcda1
};

DataSetEntry iedModelds_C1_LLN0_rmxu_fcda1 = {
  "C1",
  false,
  "RMXU1$MX$AmpLocPhsB",
  -1,
  NULL,
  NULL,
  &iedModelds_C1_LLN0_rmxu_fcda2
};

DataSetEntry iedModelds_C1_LLN0_rmxu_fcda2 = {
  "C1",
  false,
  "RMXU1$MX$AmpLocPhsC",
  -1,
  NULL,
  NULL,
  NULL
};

DataSet iedModelds_C1_LLN0_rmxu = {
  "C1",
  "LLN0$rmxu",
  3,
  &iedModelds_C1_LLN0_rmxu_fcda0,
  NULL
};

LogicalDevice iedModel_C1 = {
    LogicalDeviceModelType,
    "C1",
    (ModelNode*) &iedModel,
    NULL,
    (ModelNode*) &iedModel_C1_LLN0
};

LogicalNode iedModel_C1_LLN0 = {
    LogicalNodeModelType,
    "LLN0",
    (ModelNode*) &iedModel_C1,
    (ModelNode*) &iedModel_C1_RMXU1,
    (ModelNode*) &iedModel_C1_LLN0_Mod,
};

DataObject iedModel_C1_LLN0_Mod = {
    DataObjectModelType,
    "Mod",
    (ModelNode*) &iedModel_C1_LLN0,
    (ModelNode*) &iedModel_C1_LLN0_Health,
    (ModelNode*) &iedModel_C1_LLN0_Mod_ctlVal,
    0
};

DataAttribute iedModel_C1_LLN0_Mod_ctlVal = {
    DataAttributeModelType,
    "ctlVal",
    (ModelNode*) &iedModel_C1_LLN0_Mod,
    (ModelNode*) &iedModel_C1_LLN0_Mod_stVal,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_C1_LLN0_Mod_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_C1_LLN0_Mod,
    (ModelNode*) &iedModel_C1_LLN0_Mod_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_C1_LLN0_Mod_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_C1_LLN0_Mod,
    (ModelNode*) &iedModel_C1_LLN0_Mod_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_C1_LLN0_Mod_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_C1_LLN0_Mod,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_C1_LLN0_Health = {
    DataObjectModelType,
    "Health",
    (ModelNode*) &iedModel_C1_LLN0,
    (ModelNode*) &iedModel_C1_LLN0_Beh,
    (ModelNode*) &iedModel_C1_LLN0_Health_stVal,
    0
};

DataAttribute iedModel_C1_LLN0_Health_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_C1_LLN0_Health,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_C1_LLN0_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_C1_LLN0,
    (ModelNode*) &iedModel_C1_LLN0_NamPlt,
    (ModelNode*) &iedModel_C1_LLN0_Beh_stVal,
    0
};

DataAttribute iedModel_C1_LLN0_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_C1_LLN0_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_C1_LLN0_NamPlt = {
    DataObjectModelType,
    "NamPlt",
    (ModelNode*) &iedModel_C1_LLN0,
    NULL,
    (ModelNode*) &iedModel_C1_LLN0_NamPlt_ldNs,
    0
};

DataAttribute iedModel_C1_LLN0_NamPlt_ldNs = {
    DataAttributeModelType,
    "ldNs",
    (ModelNode*) &iedModel_C1_LLN0_NamPlt,
    (ModelNode*) &iedModel_C1_LLN0_NamPlt_configRev,
    NULL,
    0,
    IEC61850_FC_EX,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_C1_LLN0_NamPlt_configRev = {
    DataAttributeModelType,
    "configRev",
    (ModelNode*) &iedModel_C1_LLN0_NamPlt,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

LogicalNode iedModel_C1_RMXU1 = {
    LogicalNodeModelType,
    "RMXU1",
    (ModelNode*) &iedModel_C1,
    (ModelNode*) &iedModel_C1_LPHD1,
    (ModelNode*) &iedModel_C1_RMXU1_Mod,
};

DataObject iedModel_C1_RMXU1_Mod = {
    DataObjectModelType,
    "Mod",
    (ModelNode*) &iedModel_C1_RMXU1,
    (ModelNode*) &iedModel_C1_RMXU1_Beh,
    (ModelNode*) &iedModel_C1_RMXU1_Mod_ctlVal,
    0
};

DataAttribute iedModel_C1_RMXU1_Mod_ctlVal = {
    DataAttributeModelType,
    "ctlVal",
    (ModelNode*) &iedModel_C1_RMXU1_Mod,
    (ModelNode*) &iedModel_C1_RMXU1_Mod_stVal,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_C1_RMXU1_Mod_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_C1_RMXU1_Mod,
    (ModelNode*) &iedModel_C1_RMXU1_Mod_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_C1_RMXU1_Mod_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_C1_RMXU1_Mod,
    (ModelNode*) &iedModel_C1_RMXU1_Mod_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_C1_RMXU1_Mod_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_C1_RMXU1_Mod,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_C1_RMXU1_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_C1_RMXU1,
    (ModelNode*) &iedModel_C1_RMXU1_Health,
    (ModelNode*) &iedModel_C1_RMXU1_Beh_stVal,
    0
};

DataAttribute iedModel_C1_RMXU1_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_C1_RMXU1_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_C1_RMXU1_Health = {
    DataObjectModelType,
    "Health",
    (ModelNode*) &iedModel_C1_RMXU1,
    (ModelNode*) &iedModel_C1_RMXU1_AmpLocPhsA,
    (ModelNode*) &iedModel_C1_RMXU1_Health_stVal,
    0
};

DataAttribute iedModel_C1_RMXU1_Health_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_C1_RMXU1_Health,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_C1_RMXU1_AmpLocPhsA = {
    DataObjectModelType,
    "AmpLocPhsA",
    (ModelNode*) &iedModel_C1_RMXU1,
    (ModelNode*) &iedModel_C1_RMXU1_AmpLocPhsB,
    (ModelNode*) &iedModel_C1_RMXU1_AmpLocPhsA_instMag,
    0
};

DataAttribute iedModel_C1_RMXU1_AmpLocPhsA_instMag = {
    DataAttributeModelType,
    "instMag",
    (ModelNode*) &iedModel_C1_RMXU1_AmpLocPhsA,
    (ModelNode*) &iedModel_C1_RMXU1_AmpLocPhsA_q,
    (ModelNode*) &iedModel_C1_RMXU1_AmpLocPhsA_instMag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_C1_RMXU1_AmpLocPhsA_instMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_C1_RMXU1_AmpLocPhsA_instMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_C1_RMXU1_AmpLocPhsA_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_C1_RMXU1_AmpLocPhsA,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataObject iedModel_C1_RMXU1_AmpLocPhsB = {
    DataObjectModelType,
    "AmpLocPhsB",
    (ModelNode*) &iedModel_C1_RMXU1,
    (ModelNode*) &iedModel_C1_RMXU1_AmpLocPhsC,
    (ModelNode*) &iedModel_C1_RMXU1_AmpLocPhsB_instMag,
    0
};

DataAttribute iedModel_C1_RMXU1_AmpLocPhsB_instMag = {
    DataAttributeModelType,
    "instMag",
    (ModelNode*) &iedModel_C1_RMXU1_AmpLocPhsB,
    (ModelNode*) &iedModel_C1_RMXU1_AmpLocPhsB_q,
    (ModelNode*) &iedModel_C1_RMXU1_AmpLocPhsB_instMag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_C1_RMXU1_AmpLocPhsB_instMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_C1_RMXU1_AmpLocPhsB_instMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_C1_RMXU1_AmpLocPhsB_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_C1_RMXU1_AmpLocPhsB,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataObject iedModel_C1_RMXU1_AmpLocPhsC = {
    DataObjectModelType,
    "AmpLocPhsC",
    (ModelNode*) &iedModel_C1_RMXU1,
    NULL,
    (ModelNode*) &iedModel_C1_RMXU1_AmpLocPhsC_instMag,
    0
};

DataAttribute iedModel_C1_RMXU1_AmpLocPhsC_instMag = {
    DataAttributeModelType,
    "instMag",
    (ModelNode*) &iedModel_C1_RMXU1_AmpLocPhsC,
    (ModelNode*) &iedModel_C1_RMXU1_AmpLocPhsC_q,
    (ModelNode*) &iedModel_C1_RMXU1_AmpLocPhsC_instMag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_C1_RMXU1_AmpLocPhsC_instMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_C1_RMXU1_AmpLocPhsC_instMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_C1_RMXU1_AmpLocPhsC_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_C1_RMXU1_AmpLocPhsC,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

LogicalNode iedModel_C1_LPHD1 = {
    LogicalNodeModelType,
    "LPHD1",
    (ModelNode*) &iedModel_C1,
    (ModelNode*) &iedModel_C1_CSWI1,
    (ModelNode*) &iedModel_C1_LPHD1_Mod,
};

DataObject iedModel_C1_LPHD1_Mod = {
    DataObjectModelType,
    "Mod",
    (ModelNode*) &iedModel_C1_LPHD1,
    (ModelNode*) &iedModel_C1_LPHD1_Health,
    (ModelNode*) &iedModel_C1_LPHD1_Mod_ctlVal,
    0
};

DataAttribute iedModel_C1_LPHD1_Mod_ctlVal = {
    DataAttributeModelType,
    "ctlVal",
    (ModelNode*) &iedModel_C1_LPHD1_Mod,
    (ModelNode*) &iedModel_C1_LPHD1_Mod_stVal,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_C1_LPHD1_Mod_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_C1_LPHD1_Mod,
    (ModelNode*) &iedModel_C1_LPHD1_Mod_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_C1_LPHD1_Mod_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_C1_LPHD1_Mod,
    (ModelNode*) &iedModel_C1_LPHD1_Mod_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_C1_LPHD1_Mod_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_C1_LPHD1_Mod,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_C1_LPHD1_Health = {
    DataObjectModelType,
    "Health",
    (ModelNode*) &iedModel_C1_LPHD1,
    (ModelNode*) &iedModel_C1_LPHD1_Beh,
    (ModelNode*) &iedModel_C1_LPHD1_Health_stVal,
    0
};

DataAttribute iedModel_C1_LPHD1_Health_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_C1_LPHD1_Health,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_C1_LPHD1_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_C1_LPHD1,
    (ModelNode*) &iedModel_C1_LPHD1_NamPlt,
    (ModelNode*) &iedModel_C1_LPHD1_Beh_stVal,
    0
};

DataAttribute iedModel_C1_LPHD1_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_C1_LPHD1_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_C1_LPHD1_NamPlt = {
    DataObjectModelType,
    "NamPlt",
    (ModelNode*) &iedModel_C1_LPHD1,
    (ModelNode*) &iedModel_C1_LPHD1_PhyNam,
    (ModelNode*) &iedModel_C1_LPHD1_NamPlt_ldNs,
    0
};

DataAttribute iedModel_C1_LPHD1_NamPlt_ldNs = {
    DataAttributeModelType,
    "ldNs",
    (ModelNode*) &iedModel_C1_LPHD1_NamPlt,
    (ModelNode*) &iedModel_C1_LPHD1_NamPlt_configRev,
    NULL,
    0,
    IEC61850_FC_EX,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_C1_LPHD1_NamPlt_configRev = {
    DataAttributeModelType,
    "configRev",
    (ModelNode*) &iedModel_C1_LPHD1_NamPlt,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_C1_LPHD1_PhyNam = {
    DataObjectModelType,
    "PhyNam",
    (ModelNode*) &iedModel_C1_LPHD1,
    (ModelNode*) &iedModel_C1_LPHD1_PhyHealth,
    (ModelNode*) &iedModel_C1_LPHD1_PhyNam_vendor,
    0
};

DataAttribute iedModel_C1_LPHD1_PhyNam_vendor = {
    DataAttributeModelType,
    "vendor",
    (ModelNode*) &iedModel_C1_LPHD1_PhyNam,
    (ModelNode*) &iedModel_C1_LPHD1_PhyNam_hwRev,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_C1_LPHD1_PhyNam_hwRev = {
    DataAttributeModelType,
    "hwRev",
    (ModelNode*) &iedModel_C1_LPHD1_PhyNam,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_C1_LPHD1_PhyHealth = {
    DataObjectModelType,
    "PhyHealth",
    (ModelNode*) &iedModel_C1_LPHD1,
    (ModelNode*) &iedModel_C1_LPHD1_Proxy,
    (ModelNode*) &iedModel_C1_LPHD1_PhyHealth_stVal,
    0
};

DataAttribute iedModel_C1_LPHD1_PhyHealth_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_C1_LPHD1_PhyHealth,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_INT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_C1_LPHD1_Proxy = {
    DataObjectModelType,
    "Proxy",
    (ModelNode*) &iedModel_C1_LPHD1,
    NULL,
    (ModelNode*) &iedModel_C1_LPHD1_Proxy_stVal,
    0
};

DataAttribute iedModel_C1_LPHD1_Proxy_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_C1_LPHD1_Proxy,
    (ModelNode*) &iedModel_C1_LPHD1_Proxy_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_INT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_C1_LPHD1_Proxy_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_C1_LPHD1_Proxy,
    (ModelNode*) &iedModel_C1_LPHD1_Proxy_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_C1_LPHD1_Proxy_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_C1_LPHD1_Proxy,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

LogicalNode iedModel_C1_CSWI1 = {
    LogicalNodeModelType,
    "CSWI1",
    (ModelNode*) &iedModel_C1,
    (ModelNode*) &iedModel_C1_CSWI2,
    (ModelNode*) &iedModel_C1_CSWI1_Mod,
};

DataObject iedModel_C1_CSWI1_Mod = {
    DataObjectModelType,
    "Mod",
    (ModelNode*) &iedModel_C1_CSWI1,
    (ModelNode*) &iedModel_C1_CSWI1_Health,
    (ModelNode*) &iedModel_C1_CSWI1_Mod_ctlVal,
    0
};

DataAttribute iedModel_C1_CSWI1_Mod_ctlVal = {
    DataAttributeModelType,
    "ctlVal",
    (ModelNode*) &iedModel_C1_CSWI1_Mod,
    (ModelNode*) &iedModel_C1_CSWI1_Mod_stVal,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_C1_CSWI1_Mod_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_C1_CSWI1_Mod,
    (ModelNode*) &iedModel_C1_CSWI1_Mod_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_C1_CSWI1_Mod_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_C1_CSWI1_Mod,
    (ModelNode*) &iedModel_C1_CSWI1_Mod_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_C1_CSWI1_Mod_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_C1_CSWI1_Mod,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_C1_CSWI1_Health = {
    DataObjectModelType,
    "Health",
    (ModelNode*) &iedModel_C1_CSWI1,
    (ModelNode*) &iedModel_C1_CSWI1_Beh,
    (ModelNode*) &iedModel_C1_CSWI1_Health_stVal,
    0
};

DataAttribute iedModel_C1_CSWI1_Health_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_C1_CSWI1_Health,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_C1_CSWI1_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_C1_CSWI1,
    (ModelNode*) &iedModel_C1_CSWI1_Pos,
    (ModelNode*) &iedModel_C1_CSWI1_Beh_stVal,
    0
};

DataAttribute iedModel_C1_CSWI1_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_C1_CSWI1_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_C1_CSWI1_Pos = {
    DataObjectModelType,
    "Pos",
    (ModelNode*) &iedModel_C1_CSWI1,
    (ModelNode*) &iedModel_C1_CSWI1_GrpAl,
    (ModelNode*) &iedModel_C1_CSWI1_Pos_stVal,
    0
};

DataAttribute iedModel_C1_CSWI1_Pos_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_C1_CSWI1_Pos,
    (ModelNode*) &iedModel_C1_CSWI1_Pos_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_CODEDENUM,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_C1_CSWI1_Pos_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_C1_CSWI1_Pos,
    (ModelNode*) &iedModel_C1_CSWI1_Pos_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_C1_CSWI1_Pos_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_C1_CSWI1_Pos,
    (ModelNode*) &iedModel_C1_CSWI1_Pos_ctlVal,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_C1_CSWI1_Pos_ctlVal = {
    DataAttributeModelType,
    "ctlVal",
    (ModelNode*) &iedModel_C1_CSWI1_Pos,
    NULL,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataObject iedModel_C1_CSWI1_GrpAl = {
    DataObjectModelType,
    "GrpAl",
    (ModelNode*) &iedModel_C1_CSWI1,
    NULL,
    (ModelNode*) &iedModel_C1_CSWI1_GrpAl_stVal,
    0
};

DataAttribute iedModel_C1_CSWI1_GrpAl_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_C1_CSWI1_GrpAl,
    (ModelNode*) &iedModel_C1_CSWI1_GrpAl_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_INT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_C1_CSWI1_GrpAl_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_C1_CSWI1_GrpAl,
    (ModelNode*) &iedModel_C1_CSWI1_GrpAl_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_C1_CSWI1_GrpAl_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_C1_CSWI1_GrpAl,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

LogicalNode iedModel_C1_CSWI2 = {
    LogicalNodeModelType,
    "CSWI2",
    (ModelNode*) &iedModel_C1,
    (ModelNode*) &iedModel_C1_MMXU1,
    (ModelNode*) &iedModel_C1_CSWI2_Mod,
};

DataObject iedModel_C1_CSWI2_Mod = {
    DataObjectModelType,
    "Mod",
    (ModelNode*) &iedModel_C1_CSWI2,
    (ModelNode*) &iedModel_C1_CSWI2_Health,
    (ModelNode*) &iedModel_C1_CSWI2_Mod_ctlVal,
    0
};

DataAttribute iedModel_C1_CSWI2_Mod_ctlVal = {
    DataAttributeModelType,
    "ctlVal",
    (ModelNode*) &iedModel_C1_CSWI2_Mod,
    (ModelNode*) &iedModel_C1_CSWI2_Mod_stVal,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_C1_CSWI2_Mod_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_C1_CSWI2_Mod,
    (ModelNode*) &iedModel_C1_CSWI2_Mod_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_C1_CSWI2_Mod_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_C1_CSWI2_Mod,
    (ModelNode*) &iedModel_C1_CSWI2_Mod_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_C1_CSWI2_Mod_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_C1_CSWI2_Mod,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_C1_CSWI2_Health = {
    DataObjectModelType,
    "Health",
    (ModelNode*) &iedModel_C1_CSWI2,
    (ModelNode*) &iedModel_C1_CSWI2_Beh,
    (ModelNode*) &iedModel_C1_CSWI2_Health_stVal,
    0
};

DataAttribute iedModel_C1_CSWI2_Health_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_C1_CSWI2_Health,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_C1_CSWI2_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_C1_CSWI2,
    (ModelNode*) &iedModel_C1_CSWI2_Pos,
    (ModelNode*) &iedModel_C1_CSWI2_Beh_stVal,
    0
};

DataAttribute iedModel_C1_CSWI2_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_C1_CSWI2_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_C1_CSWI2_Pos = {
    DataObjectModelType,
    "Pos",
    (ModelNode*) &iedModel_C1_CSWI2,
    (ModelNode*) &iedModel_C1_CSWI2_GrpAl,
    (ModelNode*) &iedModel_C1_CSWI2_Pos_stVal,
    0
};

DataAttribute iedModel_C1_CSWI2_Pos_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_C1_CSWI2_Pos,
    (ModelNode*) &iedModel_C1_CSWI2_Pos_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_CODEDENUM,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_C1_CSWI2_Pos_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_C1_CSWI2_Pos,
    (ModelNode*) &iedModel_C1_CSWI2_Pos_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_C1_CSWI2_Pos_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_C1_CSWI2_Pos,
    (ModelNode*) &iedModel_C1_CSWI2_Pos_ctlVal,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_C1_CSWI2_Pos_ctlVal = {
    DataAttributeModelType,
    "ctlVal",
    (ModelNode*) &iedModel_C1_CSWI2_Pos,
    NULL,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataObject iedModel_C1_CSWI2_GrpAl = {
    DataObjectModelType,
    "GrpAl",
    (ModelNode*) &iedModel_C1_CSWI2,
    NULL,
    (ModelNode*) &iedModel_C1_CSWI2_GrpAl_stVal,
    0
};

DataAttribute iedModel_C1_CSWI2_GrpAl_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_C1_CSWI2_GrpAl,
    (ModelNode*) &iedModel_C1_CSWI2_GrpAl_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_INT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_C1_CSWI2_GrpAl_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_C1_CSWI2_GrpAl,
    (ModelNode*) &iedModel_C1_CSWI2_GrpAl_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_C1_CSWI2_GrpAl_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_C1_CSWI2_GrpAl,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

LogicalNode iedModel_C1_MMXU1 = {
    LogicalNodeModelType,
    "MMXU1",
    (ModelNode*) &iedModel_C1,
    (ModelNode*) &iedModel_C1_TVTR1,
    (ModelNode*) &iedModel_C1_MMXU1_Mod,
};

DataObject iedModel_C1_MMXU1_Mod = {
    DataObjectModelType,
    "Mod",
    (ModelNode*) &iedModel_C1_MMXU1,
    (ModelNode*) &iedModel_C1_MMXU1_Beh,
    (ModelNode*) &iedModel_C1_MMXU1_Mod_ctlVal,
    0
};

DataAttribute iedModel_C1_MMXU1_Mod_ctlVal = {
    DataAttributeModelType,
    "ctlVal",
    (ModelNode*) &iedModel_C1_MMXU1_Mod,
    (ModelNode*) &iedModel_C1_MMXU1_Mod_stVal,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_C1_MMXU1_Mod_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_C1_MMXU1_Mod,
    (ModelNode*) &iedModel_C1_MMXU1_Mod_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_C1_MMXU1_Mod_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_C1_MMXU1_Mod,
    (ModelNode*) &iedModel_C1_MMXU1_Mod_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_C1_MMXU1_Mod_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_C1_MMXU1_Mod,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_C1_MMXU1_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_C1_MMXU1,
    (ModelNode*) &iedModel_C1_MMXU1_Health,
    (ModelNode*) &iedModel_C1_MMXU1_Beh_stVal,
    0
};

DataAttribute iedModel_C1_MMXU1_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_C1_MMXU1_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_C1_MMXU1_Health = {
    DataObjectModelType,
    "Health",
    (ModelNode*) &iedModel_C1_MMXU1,
    (ModelNode*) &iedModel_C1_MMXU1_Amps,
    (ModelNode*) &iedModel_C1_MMXU1_Health_stVal,
    0
};

DataAttribute iedModel_C1_MMXU1_Health_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_C1_MMXU1_Health,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_C1_MMXU1_Amps = {
    DataObjectModelType,
    "Amps",
    (ModelNode*) &iedModel_C1_MMXU1,
    (ModelNode*) &iedModel_C1_MMXU1_Volts,
    (ModelNode*) &iedModel_C1_MMXU1_Amps_mag,
    0
};

DataAttribute iedModel_C1_MMXU1_Amps_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_C1_MMXU1_Amps,
    (ModelNode*) &iedModel_C1_MMXU1_Amps_q,
    (ModelNode*) &iedModel_C1_MMXU1_Amps_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_C1_MMXU1_Amps_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_C1_MMXU1_Amps_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_C1_MMXU1_Amps_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_C1_MMXU1_Amps,
    (ModelNode*) &iedModel_C1_MMXU1_Amps_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_C1_MMXU1_Amps_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_C1_MMXU1_Amps,
    (ModelNode*) &iedModel_C1_MMXU1_Amps_sVC,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_C1_MMXU1_Amps_sVC = {
    DataAttributeModelType,
    "sVC",
    (ModelNode*) &iedModel_C1_MMXU1_Amps,
    (ModelNode*) &iedModel_C1_MMXU1_Amps_int1,
    (ModelNode*) &iedModel_C1_MMXU1_Amps_sVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_C1_MMXU1_Amps_sVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_C1_MMXU1_Amps_sVC,
    (ModelNode*) &iedModel_C1_MMXU1_Amps_sVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_C1_MMXU1_Amps_sVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_C1_MMXU1_Amps_sVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_C1_MMXU1_Amps_int1 = {
    DataAttributeModelType,
    "int1",
    (ModelNode*) &iedModel_C1_MMXU1_Amps,
    (ModelNode*) &iedModel_C1_MMXU1_Amps_int2,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_INT32,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_C1_MMXU1_Amps_int2 = {
    DataAttributeModelType,
    "int2",
    (ModelNode*) &iedModel_C1_MMXU1_Amps,
    (ModelNode*) &iedModel_C1_MMXU1_Amps_int3,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_INT32,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_C1_MMXU1_Amps_int3 = {
    DataAttributeModelType,
    "int3",
    (ModelNode*) &iedModel_C1_MMXU1_Amps,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_INT32,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataObject iedModel_C1_MMXU1_Volts = {
    DataObjectModelType,
    "Volts",
    (ModelNode*) &iedModel_C1_MMXU1,
    NULL,
    (ModelNode*) &iedModel_C1_MMXU1_Volts_mag,
    0
};

DataAttribute iedModel_C1_MMXU1_Volts_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_C1_MMXU1_Volts,
    (ModelNode*) &iedModel_C1_MMXU1_Volts_q,
    (ModelNode*) &iedModel_C1_MMXU1_Volts_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_C1_MMXU1_Volts_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_C1_MMXU1_Volts_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_C1_MMXU1_Volts_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_C1_MMXU1_Volts,
    (ModelNode*) &iedModel_C1_MMXU1_Volts_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_C1_MMXU1_Volts_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_C1_MMXU1_Volts,
    (ModelNode*) &iedModel_C1_MMXU1_Volts_sVC,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_C1_MMXU1_Volts_sVC = {
    DataAttributeModelType,
    "sVC",
    (ModelNode*) &iedModel_C1_MMXU1_Volts,
    (ModelNode*) &iedModel_C1_MMXU1_Volts_int1,
    (ModelNode*) &iedModel_C1_MMXU1_Volts_sVC_scaleFactor,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_C1_MMXU1_Volts_sVC_scaleFactor = {
    DataAttributeModelType,
    "scaleFactor",
    (ModelNode*) &iedModel_C1_MMXU1_Volts_sVC,
    (ModelNode*) &iedModel_C1_MMXU1_Volts_sVC_offset,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_C1_MMXU1_Volts_sVC_offset = {
    DataAttributeModelType,
    "offset",
    (ModelNode*) &iedModel_C1_MMXU1_Volts_sVC,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_C1_MMXU1_Volts_int1 = {
    DataAttributeModelType,
    "int1",
    (ModelNode*) &iedModel_C1_MMXU1_Volts,
    (ModelNode*) &iedModel_C1_MMXU1_Volts_int2,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_INT32,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_C1_MMXU1_Volts_int2 = {
    DataAttributeModelType,
    "int2",
    (ModelNode*) &iedModel_C1_MMXU1_Volts,
    (ModelNode*) &iedModel_C1_MMXU1_Volts_int3,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_INT32,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_C1_MMXU1_Volts_int3 = {
    DataAttributeModelType,
    "int3",
    (ModelNode*) &iedModel_C1_MMXU1_Volts,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_INT32,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

LogicalNode iedModel_C1_TVTR1 = {
    LogicalNodeModelType,
    "TVTR1",
    (ModelNode*) &iedModel_C1,
    NULL,
    (ModelNode*) &iedModel_C1_TVTR1_Mod,
};

DataObject iedModel_C1_TVTR1_Mod = {
    DataObjectModelType,
    "Mod",
    (ModelNode*) &iedModel_C1_TVTR1,
    (ModelNode*) &iedModel_C1_TVTR1_Health,
    (ModelNode*) &iedModel_C1_TVTR1_Mod_ctlVal,
    0
};

DataAttribute iedModel_C1_TVTR1_Mod_ctlVal = {
    DataAttributeModelType,
    "ctlVal",
    (ModelNode*) &iedModel_C1_TVTR1_Mod,
    (ModelNode*) &iedModel_C1_TVTR1_Mod_stVal,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_C1_TVTR1_Mod_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_C1_TVTR1_Mod,
    (ModelNode*) &iedModel_C1_TVTR1_Mod_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_C1_TVTR1_Mod_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_C1_TVTR1_Mod,
    (ModelNode*) &iedModel_C1_TVTR1_Mod_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_C1_TVTR1_Mod_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_C1_TVTR1_Mod,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_C1_TVTR1_Health = {
    DataObjectModelType,
    "Health",
    (ModelNode*) &iedModel_C1_TVTR1,
    (ModelNode*) &iedModel_C1_TVTR1_Beh,
    (ModelNode*) &iedModel_C1_TVTR1_Health_stVal,
    0
};

DataAttribute iedModel_C1_TVTR1_Health_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_C1_TVTR1_Health,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_C1_TVTR1_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_C1_TVTR1,
    (ModelNode*) &iedModel_C1_TVTR1_Vol,
    (ModelNode*) &iedModel_C1_TVTR1_Beh_stVal,
    0
};

DataAttribute iedModel_C1_TVTR1_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_C1_TVTR1_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_C1_TVTR1_Vol = {
    DataObjectModelType,
    "Vol",
    (ModelNode*) &iedModel_C1_TVTR1,
    NULL,
    (ModelNode*) &iedModel_C1_TVTR1_Vol_instMag,
    0
};

DataAttribute iedModel_C1_TVTR1_Vol_instMag = {
    DataAttributeModelType,
    "instMag",
    (ModelNode*) &iedModel_C1_TVTR1_Vol,
    (ModelNode*) &iedModel_C1_TVTR1_Vol_q,
    (ModelNode*) &iedModel_C1_TVTR1_Vol_instMag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_C1_TVTR1_Vol_instMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_C1_TVTR1_Vol_instMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_C1_TVTR1_Vol_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_C1_TVTR1_Vol,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

extern ReportControlBlock iedModel_C1_LLN0_report0;
extern ReportControlBlock iedModel_C1_LLN0_report1;
extern ReportControlBlock iedModel_C1_LLN0_report2;
extern ReportControlBlock iedModel_C1_LLN0_report3;
extern ReportControlBlock iedModel_C1_LLN0_report4;
extern ReportControlBlock iedModel_C1_LLN0_report5;
extern ReportControlBlock iedModel_C1_LLN0_report6;
extern ReportControlBlock iedModel_C1_LLN0_report7;
extern ReportControlBlock iedModel_C1_LLN0_report8;
extern ReportControlBlock iedModel_C1_LLN0_report9;

ReportControlBlock iedModel_C1_LLN0_report0 = {&iedModel_C1_LLN0, "PosReport01", NULL, false, "Positions", 1, 19, 32, 0, 0, {0x4, 0x7f, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_C1_LLN0_report1};
ReportControlBlock iedModel_C1_LLN0_report1 = {&iedModel_C1_LLN0, "PosReport02", NULL, false, "Positions", 1, 19, 32, 0, 0, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_C1_LLN0_report2};
ReportControlBlock iedModel_C1_LLN0_report2 = {&iedModel_C1_LLN0, "PosReport03", NULL, false, "Positions", 1, 19, 32, 0, 0, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_C1_LLN0_report3};
ReportControlBlock iedModel_C1_LLN0_report3 = {&iedModel_C1_LLN0, "PosReport04", NULL, false, "Positions", 1, 19, 32, 0, 0, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_C1_LLN0_report4};
ReportControlBlock iedModel_C1_LLN0_report4 = {&iedModel_C1_LLN0, "PosReport05", NULL, false, "Positions", 1, 19, 32, 0, 0, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_C1_LLN0_report5};
ReportControlBlock iedModel_C1_LLN0_report5 = {&iedModel_C1_LLN0, "MeaReport01", NULL, false, "Measurands", 1, 26, 36, 0, 2000, {0x4, 0x7f, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_C1_LLN0_report6};
ReportControlBlock iedModel_C1_LLN0_report6 = {&iedModel_C1_LLN0, "MeaReport02", NULL, false, "Measurands", 1, 26, 36, 0, 2000, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_C1_LLN0_report7};
ReportControlBlock iedModel_C1_LLN0_report7 = {&iedModel_C1_LLN0, "MeaReport03", NULL, false, "Measurands", 1, 26, 36, 0, 2000, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_C1_LLN0_report8};
ReportControlBlock iedModel_C1_LLN0_report8 = {&iedModel_C1_LLN0, "MeaReport04", NULL, false, "Measurands", 1, 26, 36, 0, 2000, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_C1_LLN0_report9};
ReportControlBlock iedModel_C1_LLN0_report9 = {&iedModel_C1_LLN0, "MeaReport05", NULL, false, "Measurands", 1, 26, 36, 0, 2000, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, NULL};

extern SVControlBlock iedModel_C1_LLN0_smv0;
extern SVControlBlock iedModel_C1_LLN0_smv1;
extern SVControlBlock iedModel_C1_LLN0_smv2;

static PhyComAddress iedModel_C1_LLN0_smv0_address = {
  4,
  291,
  16384,
  {0x1, 0xc, 0xcd, 0x4, 0x0, 0x1}
};

SVControlBlock iedModel_C1_LLN0_smv0 = {&iedModel_C1_LLN0, "PerformanceSV", "Performance", "Performance", 7, 0, 4800, 1, &iedModel_C1_LLN0_smv0_address, false, 1, &iedModel_C1_LLN0_smv1};

static PhyComAddress iedModel_C1_LLN0_smv1_address = {
  4,
  291,
  16384,
  {0x1, 0xc, 0xcd, 0x4, 0x0, 0x1}
};

SVControlBlock iedModel_C1_LLN0_smv1 = {&iedModel_C1_LLN0, "Volt", "11", "smv", 7, 0, 4800, 1, &iedModel_C1_LLN0_smv1_address, false, 2, &iedModel_C1_LLN0_smv2};

static PhyComAddress iedModel_C1_LLN0_smv2_address = {
  4,
  291,
  16384,
  {0x1, 0xc, 0xcd, 0x4, 0x0, 0x1}
};

SVControlBlock iedModel_C1_LLN0_smv2 = {&iedModel_C1_LLN0, "rmxuCB", "rmxu", "rmxu", 7, 0, 16, 1, &iedModel_C1_LLN0_smv2_address, false, 16, NULL};

extern GSEControlBlock iedModel_C1_LLN0_gse0;
extern GSEControlBlock iedModel_C1_LLN0_gse1;
extern GSEControlBlock iedModel_C1_LLN0_gse2;

static PhyComAddress iedModel_C1_LLN0_gse0_address = {
  4,
  0,
  12288,
  {0x1, 0xc, 0xcd, 0x1, 0x0, 0x4}
};

GSEControlBlock iedModel_C1_LLN0_gse0 = {&iedModel_C1_LLN0, "Performance", "Performance", "Performance", 1, false, &iedModel_C1_LLN0_gse0_address, -1, -1, &iedModel_C1_LLN0_gse1};

static PhyComAddress iedModel_C1_LLN0_gse1_address = {
  4,
  0,
  12288,
  {0x1, 0xc, 0xcd, 0x1, 0x0, 0x4}
};

GSEControlBlock iedModel_C1_LLN0_gse1 = {&iedModel_C1_LLN0, "ItlPositions", "Itl", "Positions", 1, false, &iedModel_C1_LLN0_gse1_address, -1, -1, &iedModel_C1_LLN0_gse2};

static PhyComAddress iedModel_C1_LLN0_gse2_address = {
  4,
  0,
  12288,
  {0x1, 0xc, 0xcd, 0x1, 0x0, 0x4}
};

GSEControlBlock iedModel_C1_LLN0_gse2 = {&iedModel_C1_LLN0, "AnotherPositions", "Itl", "Positions", 1, false, &iedModel_C1_LLN0_gse2_address, -1, -1, NULL};


extern LogControlBlock iedModel_C1_LLN0_lcb0;
LogControlBlock iedModel_C1_LLN0_lcb0 = {&iedModel_C1_LLN0, "Log", "Positions", "C1/LLN0$C1", 3, 0, true, true, NULL};



IedModel iedModel = {
    "E1Q1SB1",
    &iedModel_C1,
    &iedModelds_C1_LLN0_Performance,
    &iedModel_C1_LLN0_report0,
    &iedModel_C1_LLN0_gse0,
    &iedModel_C1_LLN0_smv0,
    NULL,
    &iedModel_C1_LLN0_lcb0,
    NULL,
    initializeValues
};

static void
initializeValues()
{

iedModel_C1_LLN0_NamPlt_ldNs.mmsValue = MmsValue_newVisibleString("IEC61850-7-4:2003");

iedModel_C1_LLN0_NamPlt_configRev.mmsValue = MmsValue_newVisibleString("Rev 3.45");

iedModel_C1_RMXU1_AmpLocPhsA_instMag_f.mmsValue = MmsValue_newFloat(1.024);

iedModel_C1_RMXU1_AmpLocPhsB_instMag_f.mmsValue = MmsValue_newFloat(1.024);

iedModel_C1_RMXU1_AmpLocPhsC_instMag_f.mmsValue = MmsValue_newFloat(1.024);

iedModel_C1_LPHD1_NamPlt_ldNs.mmsValue = MmsValue_newVisibleString("IEC61850-7-4:2003");

iedModel_C1_LPHD1_NamPlt_configRev.mmsValue = MmsValue_newVisibleString("Rev 3.45");

iedModel_C1_LPHD1_PhyNam_vendor.mmsValue = MmsValue_newVisibleString("myVendorName");

iedModel_C1_LPHD1_PhyNam_hwRev.mmsValue = MmsValue_newVisibleString("replacement string");

iedModel_C1_LPHD1_Proxy_stVal.mmsValue = MmsValue_newIntegerFromInt32(17);

iedModel_C1_CSWI1_GrpAl_stVal.mmsValue = MmsValue_newIntegerFromInt32(178);

iedModel_C1_CSWI2_GrpAl_stVal.mmsValue = MmsValue_newIntegerFromInt32(178);

iedModel_C1_MMXU1_Amps_mag_f.mmsValue = MmsValue_newFloat(1.024);

iedModel_C1_MMXU1_Volts_mag_f.mmsValue = MmsValue_newFloat(1.024);

iedModel_C1_MMXU1_Volts_sVC_scaleFactor.mmsValue = MmsValue_newFloat(200.0);

iedModel_C1_MMXU1_Volts_sVC_offset.mmsValue = MmsValue_newFloat(10.0);

iedModel_C1_TVTR1_Vol_instMag_f.mmsValue = MmsValue_newFloat(1.024);
}
