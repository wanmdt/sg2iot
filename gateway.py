#-*- coding: utf-8 -*-
from flask import Flask, request, jsonify, make_response, abort
from flask_request_params import bind_request_params
import requests #Tem que instalar pip install requests
import paho.mqtt.publish as publish # pip install paho-mqtt
import time
import json
import datetime 
import threading
import re
import gc 


#flask variables
app = Flask(__name__)
app.before_request(bind_request_params)


@app.errorhandler(404)
def not_found(error):
    # custom error message
    return make_response(jsonify({'error': 'Not found'}), 404)


@app.errorhandler(400)
def bad_request(error):
    return make_response(jsonify({'error': 'bad request'}), 400)


# with open('IEDSpecs.json') as json_file:
#     data = json.load(json_file)
    # print("Executando com as seguintes configs:")
    # print(data["configuracao"]["IED"]["device_id"])
    # print(data["configuracao"]["IED"]["entity_type"])
    # print(data["configuracao"]["IED"]["endereco_fiware"])
    # print(data["configuracao"]["IED"]["mqtt_topico_fiware"])
    # print(data["configuracao"]["IED"]["entity_name"])
    # print(data["configuracao"]["IED"]["fiware-servicepath"])
    # print(data["configuracao"]["IED"]["fiware-service"])
    # print(data["configuracao"]["IED"]["Content-Type"])
    # print(data["configuracao"]["IED"]["transport"])
    # print(data["configuracao"]["IED"]["url"])
    
di = {}
# di[(data["configuracao"]["IED"][i]['id'])] = 'True'
# if ( not(di.has_key((data["configuracao"]["IED"][i]['id']))) or (di.get(data["configuracao"]["IED"][i]['id']) == "False") ):


def registrar_time_message(mens):
    arquivo = open("Gateway.txt", "a")
    #dataT = datetime.datetime.now()
    arquivo.write(str(datetime.datetime.now())+' - '+mens+'\n')
    arquivo.close()

# def saida_message(mens):
#     arquivo = open("SAIDA.txt", "a")
#     #dataT = datetime.datetime.now()
#     arquivo.write(mens+'\n')
#     arquivo.close()


def create_IED_Fiware(messageTotal):
    
    # with open(file_json) as json_file:
    #     data = json.load(json_file)

    try:
        cabecalhos = {'fiware-servicepath': messageTotal[9].strip(), 'fiware-service': messageTotal[10].strip()}

        #url2 = "http://10.7.229.35:4041/iot/devices/SensorDNP3"
        url2 = messageTotal[8].strip()

        r = requests.get(url2, headers=cabecalhos) #verificar se device existe
        if (r.status_code == 404): #encontrou device com mesmo nome/ criar outro device

            stringdata = '{"devices":[{"device_id":"'+messageTotal[4].strip()+'","entity_name":"'+messageTotal[5].strip()+'","entity_type":"'+messageTotal[3].strip()+'","protocol":"'+messageTotal[12].strip()+'","transport":"'+messageTotal[14].strip()+'","timezone":"'+messageTotal[13].strip()+'","attributes":[' + messageTotal[15].strip() + ']}]}'
            # stringdata = stringdata + messageTotal[15].strip() + ']}]}'

            # deviceAtri = stringdata #device + variaveis

            cabecalhosPost = {'fiware-servicepath': messageTotal[9].strip(), 'fiware-service': messageTotal[10].strip(),'Content-Type': messageTotal[11].strip()}

            try:
                urlHTTP = 'http://'+messageTotal[6].strip()+':4041/iot/devices/'
                urlpost = urlHTTP
                # 'http://10.7.229.210:4041/iot/devices/'
                #post = requests.post('http://10.7.229.210:4041/iot/devices/', headers=cabecalhosPost, data=stringdata)
                postF = requests.post(urlpost, headers=cabecalhosPost, data=stringdata)
                if postF.status_code == 409: # device exist
                    print("ERRO: A device with the same name: "+messageTotal[4].strip())
                    print(postF.status_code)
                elif postF.status_code == 201: #create device
                    print("Device created")
                    print(postF.status_code)
            except requests.exceptions.RequestException as e:  # This is the correct syntax
                print (e)
        elif r.status_code == 200: #device existe
            print("eeERRO: A device with the same name - change name: "+messageTotal[4].strip())

    except requests.exceptions.RequestException as e:  # This is the correct syntax
        print (e)

    # json_file.close()

    


def send_data_Fiware(messageTotal):

    if (messageTotal[14].strip() == "HTTP"):
        cabecalhosPost1 = {'fiware-servicepath': messageTotal[9].strip(), 'fiware-service':  messageTotal[10].strip()}

        #url = "http://10.7.229.35:7896/iot/d?k=4jggokgpepnvsb2uv4s40d59ov&i=SensorT50" 

        urlHTTP = 'http://'+messageTotal[6].strip()+':7896/iot/d?k=4jggokgpepnvsb2uv4s40d59ov&i='+messageTotal[4].strip()
        url = urlHTTP

        # porta = 5000 + ((int)(data["configuracao"]["IED"][i]["id"])+1)
        # portStr = (str(porta))
        # portStr = (str(porta))
        # dadoString_ALL = ''
        # dadoString_ALL_DA = ''
        # dadoString_DNP3 = ''

        #IEC
        if (messageTotal[3].strip() == "IEC61850"):
            
            #print('print 16 ' +messageTotal[16].strip())
            try: 
                # dadoString_ALL = messageTotal[16].strip()
                if messageTotal[16].strip() != '':
                    # dadoString_ALL_DA = messageTotal[17].strip()

                    try:
                        post1 = requests.post(url, headers=cabecalhosPost1, data=messageTotal[16].strip())
                        if post1.status_code == 200:
                            print("IEC61850 - Sent ALL via HTTP "+ messageTotal[4].strip())
                        else:
                            print(post1.status_code, " ERRO sending data to "+ messageTotal[4].strip())
                    except requests.exceptions.RequestException as e:  # This is the correct syntax
                        print (e)
            except :
                print()
                

        elif(messageTotal[3].strip() == "DNP3"):
            try:
                # dadoString_DNP3 = messageTotal[16].strip()
                if messageTotal[16].strip() != '':
                    try:
                        post1 = requests.post(url, headers=cabecalhosPost1, data=messageTotal[16].strip())
                        if post1.status_code == 200:
                            print(" DNP3 - Sent via HTTP "+ messageTotal[4].strip())
                        else:
                            print(post1.status_code," ERRO sending data to "+ messageTotal[4].strip())
                    except requests.exceptions.RequestException as e:  # This is the correct syntax
                        print (e)
            except :
                print()



    elif (messageTotal[14].strip() == "MQTT"):

        # dadoString_ALL = ''
        # dadoString_ALL_DA = ''
        # dadoString_DNP3 = ''
        # fiware_broker = messageTotal[6].strip()
        # topicDNP3 = messageTotal[7].strip()

        if (messageTotal[3].strip() == "IEC61850"):
            try:
                # dadoString_ALL = messageTotal[16].strip()
                #dadoString_ALL_DA = messageTotal[17].strip()
                if messageTotal[16].strip() != '':

                    publish.single(messageTotal[7].strip(), messageTotal[16].strip(), hostname=messageTotal[6].strip())
                    print("IEC61850 - Sent ALL via MQTT "+ messageTotal[4].strip())
                else:
                    print(" ERRO sending data to "+ messageTotal[4].strip())
            except :
                print()


        elif(messageTotal[3].strip() == "DNP3"):

            try:            
                # dadoString_DNP3 = messageTotal[16].strip()
                if messageTotal[16].strip() != '':
                    
                    publish.single(messageTotal[7].strip(), messageTotal[16].strip(), hostname=messageTotal[6].strip())
                    print("DNP3 - Sent via MQTT "+ messageTotal[4].strip())
                else:
                    print(" ERRO sending data to "+ messageTotal[4].strip())
            except :
                print()



@app.route('/gateway/DATA/', methods=['GET', 'POST'])
def data():
    print(gc.get_count())
    stringg = ''

    if request.method == 'POST':
        # stringg = request.data
        # result = re.split(r'\w+\:\s', stringg.strip())
        result = re.split(r'\w+\:\s', request.data.strip())
        
        seq = request.args.get('seq')

        mens = ' ReceiveDATAfromDH - ID: '+result[1].strip()+' seq: '+str(seq)+' length: '+str(len(request.data))+' deviceip: '+result[2].strip()+' entitytype: '+result[3].strip()
        registrar_time_message(mens) #save txt
        # string_JSON_TOTAL = stringg
        # print(string_JSON_TOTAL)

        try:
            #result = re.split(r'\w+\:\s', stringg.strip())

            # check se variavel ja foi criada pelo id
            if ( not(di.has_key((result[1].strip())) or (di.get(result[1].strip()) == "False"))):
                di[(result[1].strip())] = 'True'
                create_IED_Fiware(result)
                print(di)
            
            
            
            #time.sleep(1)
            send_data_Fiware(result)
            #for age in range(0, len(result)):
	            # print(age,result[age].strip())
                #saida_message(str(age)+' '+str(result[age].strip()))
            # print('testando',len(result))
            #saida_message('T'+str(len(result)))
            
            if len(result) == 16:
                tam = str(len(result[15].strip()))
                # for aa in range(0,3):
                    # saida_message('1OUT')
            else:
                tam = str(len(result[16].strip()))


            # if result[16].strip() != not_found:
            #     tam = str(len(result[16].strip()))
            # else:
            #     tam = '0'
            mens = ' SentDATAFIWARE - ID: '+result[1].strip()+' seq: '+str(seq)+' length: '+tam+' deviceip: '+result[2].strip()+' entitytype: '+result[3].strip()
            registrar_time_message(mens) #save txt
            mens = ''

        except requests.exceptions.RequestException as e:  # This is the correct syntax
            print (e)

    # print(stringg)
    return stringg
    if request.method == 'GET':
        stringg = "Not permited"
        return stringg
    


   

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5015)
