from flask import Flask, request, jsonify, make_response, abort
from flask_request_params import bind_request_params
from werkzeug.utils import secure_filename
import requests
import json
import os


# url2 = "http://127.0.0.1:5005/datahandler/IEDDG/DA/"

# r = requests.get(url2)
# print(r.status_code)
# data = r.text
# print(data)

# UPLOAD_FOLDER = '/path/to/the/uploads'
ALLOWED_EXTENSIONS = {'txt', 'json'}


# flask variables
app = Flask(__name__)
app.before_request(bind_request_params)
# app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.errorhandler(404)
def not_found(error):
    #custom error message
    return make_response(jsonify({'error': 'Not found'}), 404)


@app.errorhandler(400)
def bad_request(error):
        return make_response(jsonify({'error': 'bad request'}), 400)


@app.route('/upload/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        if 'file' not in request.files:
            flash('No file part')
            #return request.status_code
            return "No file part"
        file = request.files['file']
        if file.filename == '':
            flash('No selected file')
            #return request.status_code
            return "No file part"
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(os.getcwd(), filename))
            #return request.status_code
            return "File Upload Successful"
    return "Implement GET"



if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)


