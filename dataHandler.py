
from flask import Flask, request, jsonify, make_response, abort
from flask_request_params import bind_request_params
import requests
import json
import re
import os
import subprocess
import time
import datetime 
import threading
import gc 

global data

global STRING_TOTAL



#flask variables
app = Flask(__name__)
app.before_request(bind_request_params)


@app.errorhandler(404)
def not_found(error):
    # custom error message
    return make_response(jsonify({'error': 'Not found'}), 404)


@app.errorhandler(400)
def bad_request(error):
    return make_response(jsonify({'error': 'bad request'}), 400)


with open('IEDSpecs.json') as json_file:
    data = json.load(json_file)


di = {}
#lista.append('Android')

def create_txt():
    arquivoD = open("DataHanler.txt", "w")
    arquivoD.close()

    arquivoDF = open("DataHanlerFlask.txt", "w")
    arquivoDF.close()

    arquivoIED = open("IED.txt", "w")
    arquivoIED.close()

    arquivoIED = open("Gateway.txt", "w")
    arquivoIED.close()




def registrar_time_message(mens):
    arquivo = open("DataHanler.txt", "a")
    #dataT = datetime.datetime.now()
    arquivo.write(str(datetime.datetime.now())+' - '+mens+'\n')
    arquivo.close()
    



def create_Agent(data):

    STRING_TOTAL = ' ID: '+data["configuracao"]["IED"][i]['id']+' deviceip: '+data["configuracao"]["IED"][i]['device_ip']+' entitytype: '+data["configuracao"]["IED"][i]['entity_type']+' deviceid: '+data["configuracao"]["IED"][i]['device_id']+' entityname: '+data["configuracao"]["IED"][i]['entity_name']+' enderecofiware: '+data["configuracao"]["IED"][i]['endereco_fiware']+' mqtttopicofiware: '+data["configuracao"]["IED"][i]['mqtt_topico_fiware']+' url: '+data["configuracao"]["IED"][i]['url']+' fiwareservicepath: '+data["configuracao"]["IED"][i]['fiware-servicepath']+' fiwareservice: '+data["configuracao"]["IED"][i]['fiware-service']+' ContentType: '+data["configuracao"]["IED"][i]['Content-Type']+' protocol: '+data["configuracao"]["IED"][i]['protocol']+' timezone: '+data["configuracao"]["IED"][i]['timezone']+' transport: '+data["configuracao"]["IED"][i]['transport']

    if(data["configuracao"]["IED"][i]['entity_type'] == "IEC61850"):
        ip = data["configuracao"]["IED"][i]['device_ip']

        p = (int)(data["configuracao"]["IED"][i]['id'])
        porta = 6000 + p
        portStr = (str(porta))
        di[(data["configuracao"]["IED"][i]['id'])] = 'True'
        
        #Record txt
        mens = ' AgentStart - ID: '+data["configuracao"]["IED"][i]['id']+' deviceip: '+data["configuracao"]["IED"][i]['device_ip']+' entitytype: '+data["configuracao"]["IED"][i]['entity_type']+' deviceid: '+data["configuracao"]["IED"][i]['device_id']+' enderecofiware: '+data["configuracao"]["IED"][i]['endereco_fiware']
        registrar_time_message(mens)
        mens = ''
        print (ip, STRING_TOTAL, portStr)
        proc = subprocess.Popen(['python', 'iec61850Agent.py', ip, STRING_TOTAL, portStr], stdout=subprocess.PIPE).communicate()[0]
        
    
    elif (data["configuracao"]["IED"][i]['entity_type'] == "DNP3"):
        ip = data["configuracao"]["IED"][i]['device_ip']

        p = (int)(data["configuracao"]["IED"][i]['id'])
        porta = 6000 + p
        portStr = (str(porta))
        di[(data["configuracao"]["IED"][i]['id'])] = 'True'
        
        # Record txt
        mens = ' AgentStart - ID: '+data["configuracao"]["IED"][i]['id']+' deviceip: '+data["configuracao"]["IED"][i]['device_ip']+' entitytype: '+data["configuracao"]["IED"][i]['entity_type']+' deviceid: '+data["configuracao"]["IED"][i]['device_id']+' enderecofiware: '+data["configuracao"]["IED"][i]['endereco_fiware']
        registrar_time_message(mens)
        mens = ''
        proc = subprocess.Popen(['python', 'dnp3Agent.py', ip, STRING_TOTAL, portStr], stdout=subprocess.PIPE).communicate()[0]
    
    STRING_TOTAL = ''
 
# create all the txt files for logging
create_txt()

if len(data["configuracao"]["IED"]) > 0:
    for i in range(len(data["configuracao"]["IED"])):
        thr_collection = threading.Thread(target=create_Agent,args=(data,))
        thr_collection.start()
        
        #Record txt
        mens = ' AgentStarted - ID: '+data["configuracao"]["IED"][i]['id']+' deviceip: '+data["configuracao"]["IED"][i]['device_ip']+' entitytype: '+data["configuracao"]["IED"][i]['entity_type']+' deviceid: '+data["configuracao"]["IED"][i]['device_id']+' enderecofiware: '+data["configuracao"]["IED"][i]['endereco_fiware']
        registrar_time_message(mens)
        mens = ''

        time.sleep(1)



while True:

    with open('IEDSpecs.json') as json_file1:
        data1 = json.load(json_file1)

    if (data != data1):
        # print(data == data1)
        
        for i in range(len(data["configuracao"]["IED"]),len(data1["configuracao"]["IED"])):
            thr_collection = threading.Thread(target=create_Agent, args=(data1,))
            thr_collection.start()

            #Record txt
            mens = ' AgentStarted - ID: '+data1["configuracao"]["IED"][i]['id']+' deviceip: '+data1["configuracao"]["IED"][i]['device_ip']+' entitytype: '+data1["configuracao"]["IED"][i]['entity_type']+' deviceid: '+data1["configuracao"]["IED"][i]['device_id']+' enderecofiware: '+data1["configuracao"]["IED"][i]['endereco_fiware']
            registrar_time_message(mens)
            mens = ''

            time.sleep(1)

        data = data1
        
    print(di)

    json_file1.close()
    print(gc.get_count())
    

    time.sleep(10)


    

