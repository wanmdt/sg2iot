

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdint.h>


Item *getIED(char *iedObjectRef);
Item *getLD(char *iedObjectRef, char *objectRef);
Item *getLN(char *iedObjectRef, char *LDObjectRef, char *objectRef);

/**
 * Find an item within the specified Logical Node object hierarchy.
 *
 * Provide a list of item names (where num is the number), e.g., getItem(ln, 3, "C1", "LN0", "Mod").
 */
Item *getItem(Item *ln, int num, ...);

/**
 * Converts data object reference (DataObjectRef), e.g., "myLD/MMXU1.PhV.phsA", to the database Item, if found. Returns NULL otherwise.
 */
Item *getItemFromPath(char *iedObjectRef, char *objectRefPath);

/**
 * Sets the value of leaf data items. Returns the number of bytes written if successful, or 0 otherwise.
 */
int setItem(Item *item, char *input, int input_len);

/**
 * Prints leaf data items to the specified buffer. The buffer must be large enough. Returns the number of characters printed.
 */
int itemToJSON(char *buf, Item *item);

/**
 * Prints hierarchy of items without whitespace, starting from the root, to the specified buffer.
 *
 * The buffer must be large enough. Returns the number of characters printed.
 */
int itemTreeToJSON(char *buf, Item *root);

/**
 * Prints hierarchy of items with whitespace, starting from the root, to the specified buffer.
 *
 * The buffer must be large enough. Returns the number of characters printed.
 */
int itemTreeToJSONPretty(char *buf, Item *root);

/**
 * Prints a self-descriptive hierarchy of items with whitespace, starting from the root, to the specified buffer.
 *
 * The buffer must be large enough. Returns the number of characters printed.
 */
int itemDescriptionTreeToJSONPretty(char *buf, Item *root, unsigned char deep);

/**
 * Prints a self-descriptive hierarchy of items without whitespace, starting from the root, to the specified buffer.
 *
 * The buffer must be large enough. Returns the number of characters printed.
 */
int itemDescriptionTreeToJSON(char *buf, Item *root, unsigned char deep);




