import sys
import os
import subprocess
import re
import threading


def run_web():
    try:
        proc = subprocess.Popen(['python', 'webIEDInfo.py'], stdout=subprocess.PIPE).communicate()[0]
    except OSError as e:
        print (e)

def run_gateway():
    try:
        proc = subprocess.Popen(['python', 'gateway.py'], stdout=subprocess.PIPE).communicate()[0]
    except OSError as e:
        print (e)

def run_flaskDataHandler():
    try:
        proc = subprocess.Popen(['python', 'dataHandlerFlask.py'], stdout=subprocess.PIPE).communicate()[0]
    except OSError as e:
        print (e)

def run_DataHandler():
    try:
        proc = subprocess.Popen(['python', 'dataHandler.py'], stdout=subprocess.PIPE).communicate()[0]
    except OSError as e:
        print (e)


thr_collection = threading.Thread(target=run_web)
thr_collection.start()

thr_collection1 = threading.Thread(target=run_gateway)
thr_collection1.start()

thr_collection2 = threading.Thread(target=run_flaskDataHandler)
thr_collection2.start()

thr_collection3 = threading.Thread(target=run_DataHandler)
thr_collection3.start()



