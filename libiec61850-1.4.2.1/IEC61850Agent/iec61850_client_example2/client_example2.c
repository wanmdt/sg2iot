/*
 * client_example2.c
 *
 * This example shows how to browse the data model of an unknown device.
 */

#include "iec61850_client.h"
#include <time.h>
#include <stdlib.h>
#include <stdio.h>

void
printSpaces(int spaces)
{
    int i;

    for (i = 0; i < spaces; i++)
        printf(" ");
}

void
printDataDirectory(char* doRef, IedConnection con, int spaces)
{
    IedClientError error;

    LinkedList dataAttributes = IedConnection_getDataDirectory(con, &error, doRef);

    //LinkedList dataAttributes = IedConnection_getDataDirectoryByFC(con, &error, doRef, MX);

    ClientReportControlBlock rcb = NULL;
    /* Read RCB values */
        //rcb = IedConnection_getRCBValues(con, &error, "simpleIOGenericIO/LLN0.RP.AnalogValuesRCB01", NULL);
    
    if (dataAttributes != NULL) {
        LinkedList dataAttribute = LinkedList_getNext(dataAttributes);
        

        while (dataAttribute != NULL) {

            char* daName = (char*) dataAttribute->data;

            char* FC = "ST";

            char dRef[130];
            sprintf(dRef, "%s.%s", doRef, daName);

           /* IEC61850_FC_MX && IEC61850_FC_ST 
            && IEC61850_FC_SP && IEC61850_FC_SV && IEC61850_FC_CF && IEC61850_FC_DC && IEC61850_FC_SG && IEC61850_FC_SE && IEC61850_FC_SR &&
             IEC61850_FC_OR && IEC61850_FC_BL && IEC61850_FC_EX && IEC61850_FC_CO && IEC61850_FC_US && IEC61850_FC_MS && IEC61850_FC_RP && 
             IEC61850_FC_BR && IEC61850_FC_LG && IEC61850_FC_ALL && IEC61850_FC_NONE */

            MmsValue* value = IedConnection_readObject(con, &error, dRef, 0);
            //printf(" Testando  %s\n", dRef);
            //enum eFunctionalConstraint a = IEC61850_FC_MX;

           
       //MmsValue* repor = MmsConnection_readVariable(con, &error, dRef, daName );
            //printf ("Reporte %i\n",ClientReportControlBlock_getTrgOps(rcb));
            


                for (int i = 0; i <= 18; i++)
                {
                    
                    if (MmsValue_getType(value) == MMS_DATA_ACCESS_ERROR  )
                    {
                        value = IedConnection_readObject(con, &error, dRef, i);
                        //printf("MmsValue_getType(FOR): %i \n", MmsValue_getType(value));
                        if(i == 0) FC = "ST";
                        else if (i == 1) FC = "MX";
                        else if (i == 2) FC = "SP";
                        else if (i == 3) FC = "SV";
                        else if (i == 4) FC = "CF";
                        else if (i == 5) FC = "DC";
                        else if (i == 6) FC = "SG";
                        else if (i == 7) FC = "SE";
                        else if (i == 8) FC = "SR";
                        else if (i == 9) FC = "OR";
                        else if (i == 10) FC = "BL";
                        else if (i == 11) FC = "EX";
                        else if (i == 12) FC = "CO";
                        else if (i == 13) FC = "US";
                        else if (i == 14) FC = "MS";
                        else if (i == 15) FC = "RP";
                        else if (i == 16) FC = "BR";
                        else if (i == 17) FC = "LG";
                        else if (i == 18) FC = "GO";
                        else FC = "NONE";
                    }
                    // else
                    // {
                    //     value = IedConnection_readObject(con, &error, dRef, 1);
                    // }
                }

                //printf("MmsValue_getType(FINAL): %i FC= %s\n", MmsValue_getType(value), FC);
            // if (MmsValue_getType(value) == MMS_DATA_ACCESS_ERROR  )
            // {
            //     FC = "NONE";
                
            // }

            if (value != NULL) {

                

                // MMS_ARRAY = 0
                if (MmsValue_getType(value) == MMS_STRUCTURE){

                    MmsValue_toFloat(MmsValue_getElement(value, 0));


                    printSpaces(spaces);
                    printf("DA: %s %i MMS_STRUCTURE %s\n", (char*) dataAttribute->data,  MmsValue_getArraySize(value), FC);
                    //printf("DA: %s = %i valor MMS_STRUCTURE ... %i to float %f \n", (char*) dataAttribute->data,  MmsValue_getArraySize(value), MmsValue_getType(MmsValue_getElement(value, 0)), MmsValue_toFloat(MmsValue_getElement(value, 0)));
                
                }
                else if (MmsValue_getType(value) == MMS_BOOLEAN){

                     //printf("received binary control command: ");
                    /*if (MmsValue_getBoolean(value))
                        printf("on\n");
                    else
                        printf("off\n");*/
                    printSpaces(spaces);
                    printf("DA: %s %i BOOLEAN %s\n", (char*) dataAttribute->data,  MmsValue_getBoolean(value), FC);
                }
                else if (MmsValue_getType(value) == MMS_BIT_STRING) {

                    char dBitString[130] = "";
                    for(int i = 0; i < MmsValue_getBitStringSize(value); i ++){
                        sprintf(dBitString, "%s%d", dBitString, MmsValue_getBitStringBit(value,i));

                    }
                    
                    printSpaces(spaces);

                    printf("DA: %s %s BIT_STRING %s\n", (char*) dataAttribute->data,  dBitString, FC);
                    //printf("Failed to read value (error code: %i)\n", MmsValue_getDataAccessError(value));
                }
                else if (MmsValue_getType(value) ==  MMS_INTEGER){

                     //printf("received binary control command: ");
                    /*if (MmsValue_getBoolean(value))
                        printf("on\n");
                    else
                        printf("off\n");*/
                    printSpaces(spaces);
                    printf("DA: %s %li INTEGER %s\n", (char*) dataAttribute->data,  MmsValue_toInt64(value), FC);
                }
                else if (MmsValue_getType(value) ==  MMS_UNSIGNED ){

                    char* com = "";

                     //printf("received binary control command: ");
                    if (MmsValue_getBoolean(value))
                        com ="on";
                    else
                        com ="off";
                    printSpaces(spaces);
                    printf("DA: %s %s UNSIGNED %s\n", (char*) dataAttribute->data, com, FC);
                }
                else if (MmsValue_getType(value) == MMS_FLOAT) {
                    float fval = MmsValue_toFloat(value);
                    printSpaces(spaces);
                    printf("DA: %s %f FLOAT %s\n", (char*) dataAttribute->data, fval, FC);
                }
                else if (MmsValue_getType(value) == MMS_OCTET_STRING){
                    
                    printSpaces(spaces);
                    //printf("DA: %s %i tamanho .... %s OCTET_STRING tipo 7\n", (char*) dataAttribute->data,  MmsValue_getStringSize(value), MmsValue_toString(value));
                    printf("DA: %s %s OCTET_STRING %s\n", (char*) dataAttribute->data, MmsValue_toString(value), FC);      
                }
                else if (MmsValue_getType(value) == MMS_VISIBLE_STRING){
                    
                    printSpaces(spaces);
                    printf("DA: %s %s VISIBLE_STRING %s\n", (char*) dataAttribute->data, MmsValue_toString(value), FC);
                    
                }
                // MMS_GENERALIZED_TIME = 9
                
                // MMS_BINARY_TIME = 10
                else if (MmsValue_getType(value) == MMS_BINARY_TIME){

                    time_t current_time;
                    current_time = MmsValue_toUnixTimestamp(value);
                    
                    printSpaces(spaces);
                    printf("DA: %s %ld BINARY_TIME %s\n", (char*) dataAttribute->data, current_time, FC);
                    
                }

                // MMS_BCD = 11
                // MMS_OBJ_ID = 12
                else if (MmsValue_getType(value) == MMS_STRING){
                    
                    printSpaces(spaces);
                    printf("DA: %s %s STRING %s\n", (char*) dataAttribute->data, MmsValue_toString(value), FC);
                    
                }
                
                else if (MmsValue_getType(value) == MMS_UTC_TIME ) {

                    uint64_t timestamp = MmsValue_getUtcTimeInMs(value);

                    time_t current_time;
                    char* c_time_string;
                    /* Obtain current time. */
                    current_time = MmsValue_toUnixTimestamp(value);

                    /* Convert to local time format. */
                    c_time_string = ctime(&current_time);
                    
                    
                    //MmsValue_toUnixTimestamp(value);

                    printSpaces(spaces);
                    printf("DA: %s %ld UTC_TIME %s\n", (char*) dataAttribute->data, current_time, FC);
                    //printf("DA: %s %s UTC_TIME\n", (char*) dataAttribute->data, c_time_string);
                    //printf("Failed to read value (error code: %i)\n", MmsValue_getDataAccessError(value));
                }
                else if (MmsValue_getType(value) == MMS_DATA_ACCESS_ERROR) {

                    printSpaces(spaces);
                    printf("DA: %s %i DATA_ACCESS_ERROR %s\n", (char*) dataAttribute->data,  MmsValue_getDataAccessError(value), FC);
                    //printf("Failed to read value (error code: %i)\n", MmsValue_getDataAccessError(value));
                }
                else {

                    printSpaces(spaces);
                    printf("DA: %s %iTYPE DEFINETYPE %s\n", (char*) dataAttribute->data, MmsValue_getType(value), FC);

                }

                MmsValue_delete(value);
            }
 

            //printSpaces(spaces);
            //printf("DA: %s = AJUDa\n", (char*) dataAttribute->data);

            // int device11 = LinkedList_size(dataAttribute);
            // printf(" Testando  %i\n", device11 + 1);

            dataAttribute = LinkedList_getNext(dataAttribute);

            char daRef[130];
            sprintf(daRef, "%s.%s", doRef, daName);
            printDataDirectory(daRef, con, spaces + 2);
        }
    }

    LinkedList_destroy(dataAttributes);
}




int
main(int argc, char** argv)
{

    char* hostname;
    int tcpPort = 102;

    if (argc > 1)
        hostname = argv[1];
    else
        hostname = "localhost";

    if (argc > 2)
        tcpPort = atoi(argv[2]);

    IedClientError error;

    IedConnection con = IedConnection_create();

    IedConnection_connect(con, &error, hostname, tcpPort);

    int device1; //cont device

    if (error == IED_ERROR_OK) {

        printf("Get logical device list...\n");
        LinkedList deviceList = IedConnection_getLogicalDeviceList(con, &error);

        if (error != IED_ERROR_OK) {
            printf("Failed to read device list (error code: %i)\n", error);
            goto cleanup_and_exit;
        }

        LinkedList device = LinkedList_getNext(deviceList);
        //int device11 = LinkedList_size(deviceList);



        while (device != NULL) {
            //printf("LD: %s\n", (char*) device->data);


            LinkedList logicalNodes = IedConnection_getLogicalDeviceDirectory(con, &error,
                    (char*) device->data);

            LinkedList logicalNode = LinkedList_getNext(logicalNodes);

            device1 = LinkedList_size(logicalNode); // quantidade de LN em cada LD

            printf("LD: %s %i\n", (char*) device->data, device1 + 1);


            while (logicalNode != NULL) {
                //printf("  LN: %s\n", (char*) logicalNode->data);


                char lnRef[129];

                sprintf(lnRef, "%s/%s", (char*) device->data, (char*) logicalNode->data);
                //printf(" Testando  %s\n", lnRef);

                LinkedList dataObjects = IedConnection_getLogicalNodeDirectory(con, &error,
                        lnRef, ACSI_CLASS_DATA_OBJECT);

                LinkedList dataObject = LinkedList_getNext(dataObjects);
                device1 = LinkedList_size(dataObject); // Quantidade de DO em cada LN

                

                printf("  LN: %s %i\n", (char*) logicalNode->data, device1 + 1);

                


                while (dataObject != NULL) {
                    char* dataObjectName = (char*) dataObject->data;


                    dataObject = LinkedList_getNext(dataObject);

                    char doRef[129];

                    

                    sprintf(doRef, "%s/%s.%s", (char*) device->data, (char*) logicalNode->data, dataObjectName);
                    //printf(" Testando  %s\n", doRef);

                    LinkedList dataAttributes = IedConnection_getDataDirectory(con, &error, doRef);
                    if (dataAttributes != NULL) {
                    LinkedList dataAttribute = LinkedList_getNext(dataAttributes);
                    device1 = LinkedList_size(dataAttribute);
                    //printf(" Testando  %i\n", device1 + 1);
                    }

                    printf("    DO: %s %i\n", dataObjectName, device1 + 1);
                    LinkedList_destroy(dataAttributes);

                    printDataDirectory(doRef, con, 6);
                }

                LinkedList_destroy(dataObjects);

                LinkedList dataSets = IedConnection_getLogicalNodeDirectory(con, &error, lnRef,
                        ACSI_CLASS_DATA_SET);

                LinkedList dataSet = LinkedList_getNext(dataSets);

                while (dataSet != NULL) {
                    char* dataSetName = (char*) dataSet->data;
                    bool isDeletable;
                    char dataSetRef[130];
                    sprintf(dataSetRef, "%s.%s", lnRef, dataSetName);
                    //printf(" Testando  %s\n", dataSetRef);

                    LinkedList dataSetMembers = IedConnection_getDataSetDirectory(con, &error, dataSetRef,
                            &isDeletable);

                    if (isDeletable)
                        printf("    Data set: %s (deletable)\n", dataSetName);
                    else
                        printf("    Data set: %s (not deletable)\n", dataSetName);

                    LinkedList dataSetMemberRef = LinkedList_getNext(dataSetMembers);

                    while (dataSetMemberRef != NULL) {

                        char* memberRef = (char*) dataSetMemberRef->data;

                        printf("      %s\n", memberRef);

                        dataSetMemberRef = LinkedList_getNext(dataSetMemberRef);
                    }

                    LinkedList_destroy(dataSetMembers);

                    dataSet = LinkedList_getNext(dataSet);
                }

                LinkedList_destroy(dataSets);

                LinkedList reports = IedConnection_getLogicalNodeDirectory(con, &error, lnRef,
                        ACSI_CLASS_URCB);

                LinkedList report = LinkedList_getNext(reports);

                while (report != NULL) {
                    char* reportName = (char*) report->data;

                    printf("    RP: %s\n", reportName);

                    report = LinkedList_getNext(report);
                }

                LinkedList_destroy(reports);

                reports = IedConnection_getLogicalNodeDirectory(con, &error, lnRef,
                        ACSI_CLASS_BRCB);

                report = LinkedList_getNext(reports);

                while (report != NULL) {
                    char* reportName = (char*) report->data;

                    printf("    BR: %s\n", reportName);

                    report = LinkedList_getNext(report);
                }

                LinkedList_destroy(reports);

                logicalNode = LinkedList_getNext(logicalNode);
            }

            LinkedList_destroy(logicalNodes);

            device = LinkedList_getNext(device);

        }

        LinkedList_destroy(deviceList);

        IedConnection_close(con);

    }
    else {
        printf("Connection failed!\n");
    }

cleanup_and_exit:
    IedConnection_destroy(con);
}

